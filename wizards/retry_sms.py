from openerp import models, fields, api


class RetrySms(models.TransientModel):
    """
    Wizard used to retry the SMS.
    """
    _name = 'notify.retry_sms'

    def get_sms(self):
        sms_ids = self.env.context.get('active_model') == 'notify.sms' and self.env.context.get('active_ids') or []
        return [
            (0, 0, {'sms': sms})
            for sms in sms_ids
        ]

    sms = fields.One2many(comodel_name="notify.sms_helper", inverse_name="wizard_id", default=get_sms, readonly=True)

    @api.one
    def confirm(self):
        sms_ids = [x.sms.id for x in self.sms]
        self.env['notify.sms'].search([('id', 'in', sms_ids)]).send_notification()


class SMSHelper(models.TransientModel):
    """
    Helper Wizard used to retry the SMS.
    """
    _name = 'notify.sms_helper'
    wizard_id = fields.Many2one(comodel_name="notify.retry_sms", string="Wizard Id")
    sms = fields.Many2one(comodel_name="notify.sms", string="SMS")
