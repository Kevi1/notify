{
    'name': "Opal Notify",

    'summary': """
            Notification Module 
        """,

    'description': """
       Opal Notification Module
    """,

    'author': "Excel Labs",
    'website': "http://www.opal.al",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Opal',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'views/sms.xml',
        'views/email.xml',
        'views/views.xml',
        'cron/notify.xml',
        'wizards/retry_sms.xml',

    ]
}
