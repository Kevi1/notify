# -*- coding: utf-8 -*-

from openerp import models, fields, api
from openerp.tools.translate import _
import copy
import datetime
import dateutil.relativedelta as relativedelta
import functools
from werkzeug import urls
import requests


def replace_characters(word):
    """
    Replace the specified non-ascii characters with ascii characters.
    Args:
        word (unicode): The string that will be processed
    """
    replace_data = {
        u'\xeb': 'e',
        u'\xe7': 'c',
        u'\xfc': 'u',
        u'\xc7': 'C',
        u'\xcb': 'E',
    }

    for key, value in replace_data.iteritems():
        if key in word:
            word = word.replace(key, value)
    return word


try:
    # We use a jinja2 sandboxed environment to render mako templates.
    # Note that the rendering does not cover all the mako syntax, in particular
    # arbitrary Python statements are not accepted, and not all expressions are
    # allowed: only "public" attributes (not starting with '_') of objects may
    # be accessed.
    # This is done on purpose: it prevents incidental or malicious execution of
    # Python code that may break the security of the server.
    from jinja2.sandbox import SandboxedEnvironment

    mako_template_env = SandboxedEnvironment(
        block_start_string="<%",
        block_end_string="%>",
        variable_start_string="${",
        variable_end_string="}",
        comment_start_string="<%doc>",
        comment_end_string="</%doc>",
        line_statement_prefix="%",
        line_comment_prefix="##",
        trim_blocks=True,  # do not output newline after blocks
        autoescape=True,  # XML/HTML automatic escaping
    )
    mako_template_env.globals.update({
        'str': str,
        'quote': urls.url_quote,
        'urlencode': urls.url_encode,
        'datetime': datetime,
        'len': len,
        'abs': abs,
        'min': min,
        'max': max,
        'sum': sum,
        'filter': filter,
        'reduce': functools.reduce,
        'map': map,
        'round': round,

        # dateutil.relativedelta is an old-style class and cannot be directly
        # instanciated wihtin a jinja2 expression, so a lambda "proxy" is
        # is needed, apparently.
        'relativedelta': lambda *a, **kw: relativedelta.relativedelta(*a, **kw),
    })
    mako_safe_template_env = copy.copy(mako_template_env)
    mako_safe_template_env.autoescape = False

except ImportError:
    pass
mako_env = mako_template_env


class SMS(models.Model):
    _name = 'notify.sms'
    _inherit = "notify.notify"

    _rec_name = 'sent_to'

    sent_to = fields.Char(string="Sent to")
    rendered_sms = fields.Text(string="Rendered SMS")

    def load_template(self):
        template_id = self.template
        if not template_id:
            template_id = self.env['ir.model.data'].get_object_reference(self.module_name, self.reason + '_sms')[1]
        return self.env['notify.sms_template'].browse(template_id)

    def render_send_to(self):
        template = mako_env.from_string(self.load_template().sent_to)
        value = template.render(object=self.env[self.model_name].browse(self.record_id))
        values = value.split(',')
        for value in values:
            if value != "False":
                self.sent_to = value
                return
        self.sent_to = False

    def render_sms_body(self):
        template = mako_env.from_string(self.load_template().text)
        self.rendered_sms = replace_characters(template.render(object=self.env[self.model_name].browse(self.record_id)))

    def render_info(self):
        self.render_send_to()

        if not self.sent_to:
            self.unlink()
        else:
            self.render_sms_body()

    @api.multi
    def send_notification(self):
        # Commit state change even if an error happens
        self.write({'state': 'waiting'})
        self.env.cr.commit()

        arr = []

        for rec in self:
            arr.append({
                "from": "Tring TV",
                "to": [{
                    "number": rec.sent_to
                }],
                "body": {
                    "content": rec.rendered_sms
                },
                "reference": rec.id
            })

        json_req = {
            "messages": {
                "authentication": {
                    "producttoken": "f5416ae7-6781-422a-a841-6656e843da67"
                },
                "msg": arr
            }
        }
        if arr:
            requests.post('https://gw.cmtelecom.com/v1.0/message', json=json_req)


class SmsTemplate(models.Model):
    _name = "notify.sms_template"

    name = fields.Char(string="Template Name", required=True)
    sent_to = fields.Char(string="Receiver", required=True)
    text = fields.Text(string="Text", required=True)
