# -*- coding: utf-8 -*-

from openerp import models, _, fields, api, exceptions
import copy
import datetime
import dateutil.relativedelta as relativedelta
import functools
from werkzeug import urls
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
import base64
from sms import replace_characters

from email import encoders

try:
    # We use a jinja2 sandboxed environment to render mako templates.
    # Note that the rendering does not cover all the mako syntax, in particular
    # arbitrary Python statements are not accepted, and not all expressions are
    # allowed: only "public" attributes (not starting with '_') of objects may
    # be accessed.
    # This is done on purpose: it prevents incidental or malicious execution of
    # Python code that may break the security of the server.
    from jinja2.sandbox import SandboxedEnvironment

    mako_template_env = SandboxedEnvironment(
        block_start_string="<%",
        block_end_string="%>",
        variable_start_string="${",
        variable_end_string="}",
        comment_start_string="<%doc>",
        comment_end_string="</%doc>",
        line_statement_prefix="%",
        line_comment_prefix="##",
        trim_blocks=True,  # do not output newline after blocks
        autoescape=True,  # XML/HTML automatic escaping,
    )
    mako_template_env.filters['b64decode'] = base64.b64decode
    mako_template_env.globals.update({
        'str': str,
        'quote': urls.url_quote,
        'urlencode': urls.url_encode,
        'datetime': datetime,
        'len': len,
        'abs': abs,
        'min': min,
        'max': max,
        'sum': sum,
        'filter': filter,
        'reduce': functools.reduce,
        'map': map,
        'round': round,
        'b64decode': base64.b64decode,

        # dateutil.relativedelta is an old-style class and cannot be directly
        # instanciated wihtin a jinja2 expression, so a lambda "proxy" is
        # is needed, apparently.
        'relativedelta': lambda *a, **kw: relativedelta.relativedelta(*a, **kw),
    })
    mako_safe_template_env = copy.copy(mako_template_env)
    mako_safe_template_env.autoescape = False

except ImportError:
    pass

mako_env = mako_safe_template_env


class Email(models.Model):
    _name = "notify.email"

    _inherit = "notify.notify"

    _rec_name = 'id'

    def get_email_account(self):
        email_acc = self.env['notify.email_account'].search([], order='sequence desc', limit=1)
        if email_acc:
            return email_acc.id
        else:
            raise exceptions.ValidationError(_("Couldn't find email account!"))

    email_account = fields.Many2one(comodel_name="notify.email_account", string="Email Account",
                                    default=get_email_account)

    email_template = fields.Many2one(comodel_name='notify.email_template', string="Email Template")

    rendered_body_html = fields.Text(string="Rendered Body HTML")

    rendered_subject = fields.Char(string="Subject")

    rendered_email_to = fields.Char(string="Email To")
    rendered_email_cc = fields.Char(string="Email Cc")
    rendered_email_from = fields.Char(string="Email from")

    attachments = fields.One2many(comodel_name="notify.email_attachments", string="Attachments", inverse_name="email")

    no_attachments = fields.Integer(string="Number of Attachments", compute='_number_of_attachments', store=True)

    rendered = fields.Boolean(string="Rendered")

    @api.one
    @api.depends('attachments')
    def _number_of_attachments(self):
        if self.attachments:
            self.no_attachments = len(self.attachments)

    def load_template(self):
        if self.template:
            template_id = self.template
        else:
            template_id = self.env['ir.model.data'].get_object_reference(self.module_name, self.reason + "_email")[1]
        self.email_template = self.env['notify.email_template'].browse(template_id)

    def render_body_html(self):
        template = mako_env.from_string(self.email_template.body_html)
        self.rendered_body_html = replace_characters(template.render(object=self.env[self.model_name].browse(self.record_id),
                                                  user=self.env.user,
                                                  url=self.env['ir.config_parameter'].get_param('web.base.url')))

    def render_subject(self):
        template = mako_env.from_string(self.email_template.subject)
        self.rendered_subject = replace_characters(template.render(object=self.env[self.model_name].browse(self.record_id),
                                                user=self.env.user))

    def render_email_from(self):
        template = mako_env.from_string(self.email_template.email_from)
        self.rendered_email_from = template.render(object=self.env[self.model_name].browse(self.record_id),
                                                   user=self.env.user)

    def render_email_to(self):
        if not self.rendered_email_to:
            self.rendered_email_to = ''
        else:
            self.rendered_email_to += ','
        template = mako_env.from_string(self.email_template.email_to)
        render_txt = template.render(object=self.env[self.model_name].browse(self.record_id),
                                     user=self.env.user)
        self.rendered_email_to += render_txt

    def render_email_cc(self):
        if not self.rendered_email_cc:
            self.rendered_email_cc = ''
        else:
            self.rendered_email_cc += ','

        if self.email_template.email_cc:
            template = mako_env.from_string(self.email_template.email_cc)
            render_txt = template.render(object=self.env[self.model_name].browse(self.record_id),
                                         user=self.env.user)
            self.rendered_email_cc += render_txt
        else:
            self.rendered_email_cc = self.rendered_email_cc[:-1]

    def render_attachment_file(self, attachment):
        template = mako_env.from_string(attachment.binary_file)
        return template.render(object=self.env[self.model_name].browse(self.record_id))

    def render_attachment_report(self, attachment):
        template = mako_env.from_string(attachment.file_name)
        name = template.render(object=self.env[self.model_name].browse(self.record_id))
        return self.env['ir.attachment'].search([('name', '=', name)], limit=1)

    def render_attachment_file_filename(self, attachment):
        template = mako_env.from_string(attachment.filename)
        return template.render(object=self.env[self.model_name].browse(self.record_id))

    def generate_report(self, attachment):
        self.env['ir.actions.report'].search([('report_name', '=', attachment.report_name)]).render(self.record_id)

    def prepare_attachments(self):
        attch = []
        for attachment_template in self.email_template.attachments:
            if attachment_template.attachment_type == 'report':
                self.generate_report(attachment_template)
                file = self.render_attachment_report(attachment_template)
                attch.append((0, 0, {
                    'binary_file': file.datas,
                    'filename': self.render_attachment_file_filename(attachment_template)
                }))
            else:
                file = self.render_attachment_file(attachment_template)
                # Hack, didn't find other way to check if object is found on jinja parsing
                if 'None' not in file:
                    attch.append((0, 0, {
                        'binary_file': bytes(file[1:-1], 'utf-8'),
                        'filename': self.render_attachment_file_filename(attachment_template)
                    }))
        self.attachments = attch
        self.rendered = True

    def has_valid_email(self):
        # Checks if there is at least one valid email inside the string self.rendered_email_to and self.rendered_email_cc
        emails = list(filter(lambda x: x != 'False', self.rendered_email_to.split(',')))
        if any(emails):
            return True
        else:
            return False

    def render_info(self):
        self.load_template()
        self.render_email_to()
        self.render_email_cc()
        if not self.has_valid_email():
            self.unlink()
        else:
            self.render_subject()
            self.render_body_html()
            self.render_email_from()

    @api.multi
    def send_notification(self):
        self.write({'state': 'waiting'})
        self.env.cr.commit()

        for rec in self:
            if not rec.rendered:
                rec.prepare_attachments()
            state = rec.sudo().email_account.send(email=rec)
            if state:
                rec.state = 'successful'
            else:
                rec.state = 'unsuccessful'


class EmailAccounts(models.Model):
    _name = "notify.email_account"

    name = fields.Char("Account Identifier")
    smtp_server = fields.Char("SMTP Server")
    port_number = fields.Integer("Port Number")

    email = fields.Char("Email")
    password = fields.Char("Password")
    sequence = fields.Integer(string="Sequence")

    def get_valid_email_as_text(self, email_txt):
        return ','.join(list(filter(lambda x: x != 'False' and x, email_txt.split(','))))

    def get_valid_email_as_list(self, email_txt):
        return list(filter(lambda x: x != 'False' and x, email_txt.split(',')))

    def prepare_email(self, email):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = email.rendered_subject
        msg['From'] = email.rendered_email_from
        msg['To'] = self.get_valid_email_as_text(email.rendered_email_to)
        if self.get_valid_email_as_text(email.rendered_email_cc):
            msg['Cc'] = self.get_valid_email_as_text(email.rendered_email_cc)
        msg.add_header('reply-to', email.rendered_email_from)
        html = MIMEText(email.rendered_body_html.encode('utf-8'), 'html')
        msg.attach(html)

        return msg

    def set_attachments(self, msg, email):
        # For each attachment prepare the MIMEBase and attach them in the msg
        for attach in email.attachments:
            attachment = MIMEBase('application', 'octet-stream')
            attachment.set_payload(base64.b64decode(attach.binary_file))
            attachment.add_header('Content-Disposition', 'attachment; filename="%s"'
                                  % attach.filename)
            encoders.encode_base64(attachment)
            msg.attach(attachment)

    def send(self, email):
        try:
            msg = self.prepare_email(email)
            self.set_attachments(msg, email)
            mail = smtplib.SMTP_SSL(self.smtp_server, self.port_number)
            mail.login(self.email, self.password)
            mail.sendmail(email.rendered_email_from,
                          self.get_valid_email_as_list(email.rendered_email_to) + self.get_valid_email_as_list(
                              email.rendered_email_cc), msg.as_string())
            mail.quit()
            return True
        except:
            return False


class EmailTemplate(models.Model):
    _name = "notify.email_template"

    name = fields.Char(string="Template Name", required=True)
    body_html = fields.Text(string="HTML", required=True)
    email_from = fields.Char(string="Email From", required=True)
    email_to = fields.Char(string="Email To", required=True)
    email_cc = fields.Char(string="Email Cc")
    subject = fields.Char(string="Subject", required=True)

    attachments = fields.One2many(string='Attachments', comodel_name="notify.email_attachment_template",
                                  inverse_name='real_email_template')


class EmailAttachments(models.Model):
    _name = "notify.email_attachments"

    email = fields.Many2one(string="Email", comodel_name="notify.email")

    binary_file = fields.Binary(string="File", attachment=True, required=True)
    filename = fields.Char(string="Filename", required=True)


class EmailAttachmentsTemplate(models.Model):
    _name = 'notify.email_attachment_template'

    name = fields.Char("Attachment")
    real_email_template = fields.Many2one(string="Email Template", comodel_name="notify.email_template",
                                          compute='_compute_real_email_template', store=True)

    attachment_type = fields.Selection([
        ('report', 'Report'),
        ('uploaded_file', 'File')
    ])

    parent_email_template = fields.Char('Parent Email Template')
    parent_model_name = fields.Char(string="Parent Model Name")
    filename = fields.Char("Filename")
    binary_file = fields.Char("Binary File")
    report_name = fields.Char("Report Name")
    file_name = fields.Char("Report File Name")

    @api.one
    @api.depends('parent_email_template', 'parent_model_name')
    def _compute_real_email_template(self):
        if self.parent_email_template and self.parent_model_name:
            template_id = self.env['ir.model.data'].get_object_reference(self.parent_model_name.split('.')[0],
                                                                         self.parent_email_template)
            self.real_email_template = self.env['notify.email_template'].browse(template_id[1])
