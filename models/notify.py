# -*- coding: utf-8 -*-
from openerp import models, fields, api, _, exceptions
from openerp.models import BaseModel
import logging

_logger = logging.getLogger(__name__)


def notify(self, email=False, sms=False, reason=False, sms_template=False, email_template=False, email_to=False,
           email_cc=False):
    email_rec = None
    sms_rec = None
    if email:
        email_rec = self.env['notify.email'].sudo().create({
            'reason': reason,
            'record': self,
            'template': email_template,
            'rendered_email_to': email_to,
            'rendered_email_cc': email_cc
        })
        if not email_rec.exists():
            email_rec = False
    if sms:
        sms_rec = self.env['notify.sms'].sudo().create({
            'reason': reason,
            'template': sms_template,
            'record': self
        })
        if not sms_rec.exists():
            sms_rec = False

    return email_rec, sms_rec


BaseModel.notify = notify


class Notify(models.Model):
    _name = "notify.notify"
    _order = 'create_date desc'

    state = fields.Selection(string="State", selection=[
        ('new', 'New'),
        ('waiting', 'Waiting'),
        ('successful', 'Successfully Sent'),
        ('unsuccessful', 'Unsuccessful')], default='new')

    failure_reason = fields.Char(string="Failure Reason")

    reason = fields.Char(string="Reason")
    template = fields.Integer(string="Template Id")

    module_name = fields.Char(string="Module Name")
    model_name = fields.Char(string="Model Name")
    record_id = fields.Integer(string="Record Id")

    def set_module_record_model(self, record):
        self.record_id = record.id
        self.model_name = record._name
        self.module_name = record._name.split('.')[0]

    def _failed(self, message):
        self.state = 'unsuccessful'
        self.failure_reason = message

    def success(self):
        self.state = 'successful'

    @api.model
    def create(self, vals):
        new_notification = super(Notify, self).create(vals)
        print "Finished creating"

        # Set record information needed by the templates
        new_notification.set_module_record_model(vals['record'])
        print "Finished setting module record"

        new_notification.render_info()
        print "Render Finished"
        return new_notification

    def to_be_sent(self):
        # This can be overriden depending on the model that inherits it
        # Specify which notifications you want to be send
        return self.search([('state', '=', 'new')])

    @api.model
    def cron(self):
        # Send SMS
        self.env["notify.sms"].to_be_sent().send_notification()

        # Send Emails
        self.env["notify.email"].to_be_sent().send_notification()
