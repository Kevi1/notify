# -*- coding: utf-8 -*-
from openerp import http
import logging
from openerp.http import request
import json

_logger = logging.getLogger(__name__)


class Notify(http.Controller):
    def sms_response_change(self, sms, state, reason):
        sms.sudo().write({
            'state': state,
            'failure_reason': reason
        })

    @http.route('/web/sms_response', auth='none')
    def sms_response(self, **kwargs):
        _logger.info("====================================SMS RESPONSE =========================")
        _logger.info(kwargs)

        reference_num = kwargs.get('REFERENCE')
        if reference_num:
            sms = request.env['notify.sms'].sudo().search([('id', '=', int(reference_num))], limit=1)
            _logger.info("====================================FINDING SMS =========================")
            _logger.info(sms)

            if sms:
                _logger.info("=================================== SMS FOUND=========================",
                             kwargs.get("STATUS"))

                status_nr = int(kwargs.get("STATUS"))
                if status_nr == 1:
                    _logger.info("=================================== UNSUCCESS STATUS NR 1=========================")
                    self.sms_response_change(sms, 'unsuccessful', kwargs['STATUSDESCRIPTION'])
                elif status_nr == 2:
                    _logger.info("=================================== SUCCESS STATUS NR 2=========================")
                    self.sms_response_change(sms, 'successful', kwargs['STATUSDESCRIPTION'])
                elif status_nr == 3:
                    _logger.info("=================================== UNSUCCESS STATUS NR 3=========================")
                    self.sms_response_change(sms, 'unsuccessful', kwargs['STATUSDESCRIPTION'])
            else:
                _logger.info("=================================== SMS NOT FOUND=========================")
                with open("/opt/odoo/odoo/sms_error_not_found.txt", mode='a+') as err_msg:
                    err_msg.write(json.dumps(kwargs))
                    err_msg.write("\n\n")

        return json.dumps(200)
